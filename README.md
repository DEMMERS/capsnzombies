# Caps & Zombies



## Getting started

Let's install your 7 Days to Die Mods, for the Caps & Zombies server!
First let's get to where your Mods install to.

Hold the Windows Key down, then hit the R key. This should open the [Run Terminal]
Inside of the [Run Terminal] type in " %appdata%\7DaysToDie "
Hit OK.

You should now be in a directors that should have a Saves folder, Screenshots, logs, titlestorage, UserOptions.ini, launchersettings.json, etc..

In this folder, you will need to drag and drop the Mods folder found in the ZIP file to this directory. Once you've done this, the mods are installed!

You can now load up the game, and play!

